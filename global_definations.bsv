`include "global_parameters"

package global_definations;
import Assert :: *;
import RegFile :: *;
import GetPut :: *;
import ClientServer :: *;
import Connectable :: *;

//Basic types
typedef Bit#(`AddrSz) Address;
typedef Bit#(`DataSz) Data;
typedef Int#(`DataSz) DataInt;
typedef UInt#(`DataSz) DataUInt;
typedef Bit#(`InstrSz) Instruction;
typedef Bit#(`CInstrSz) CInstruction;
typedef Bit#(`RegIndexSz) RegIndex;

//instruction format related - decoding
typedef enum {Utype, JType, IType, BType, SType, RType} OpType deriving(Bits, Eq); //unused
//ALU
typedef enum {Add, Sub, Eq, NEq, Lt, LtU, Ge, GeU, Or, And, Xor, LLS, LRS, ARS, Mul, Div} AluOperation deriving(Bits, Eq); //operations
typedef union tagged { Data Indata; Address Pc; void Invalid; } AluOperand deriving(Bits, Eq);
typedef struct { AluOperation operation; AluOperand operand1; AluOperand operand2; } AluOp deriving(Bits, Eq);
typedef struct { Data aluout; Bool branchcond; Bool error; } AluOut deriving(Bits, Eq);
//Memory
typedef enum {Load, Store} MemOperation deriving(Bits, Eq);
typedef enum {B, H, W} DataWidth deriving(Bits, Eq);
typedef enum {S, U} DataSign deriving(Bits, Eq);
typedef struct { MemOperation operation; DataWidth datawidth; DataSign datasign; Data storedata; } MemOp deriving(Bits, Eq);
//Writeback
typedef enum {Alu, Mem, Imm} RegWrOperand deriving(Bits, Eq);
typedef struct { RegWrOperand operand; RegIndex writereg; } RegWrOp deriving(Bits, Eq);
typedef union tagged { Data WrData; Address MemAddress; void Invalid; } RegWbOrMemData deriving(Bits, Eq);
//Branch/Jump
typedef enum {Jal, Branch} PcChangeOperation deriving(Bits, Eq);
typedef struct { PcChangeOperation operation; Address baseaddress; Data offset; } PcChangeOp deriving(Bits, Eq);
//Inputs to pipeline stages
typedef struct {
                   Maybe#(AluOp) aluop;
                   Maybe#(MemOp) memop;
                   Maybe#(RegWrOp) regwrop;
                   RegWbOrMemData wrdata_or_memaddr;
                   Maybe#(PcChangeOp) pcchangeop;
               } DecodedInstruction deriving(Bits, Eq);
typedef struct {
                   Maybe#(DecodedInstruction) decoded_instr;
                   Bool data_hazard;
               } DecodeFnOut deriving(Bits, Eq);

typedef struct {
                   Maybe#(MemOp) memop;
                   Maybe#(RegWrOp) regwrop;
                   RegWbOrMemData wrdata_or_memaddr;
               } AluToMemInstruction deriving(Bits, Eq);

typedef struct {
                   DataWidth dw;
                   DataSign ds;
                   Maybe#(RegWrOp) regwrop;
                   RegWbOrMemData wrdata_or_memaddr;
               } MemToWbInstruction deriving(Bits, Eq);

//Parameters related to interfaces
typedef enum { Read, Write } ReadWrite deriving(Bits, Eq);
typedef struct { ReadWrite rw; Address addr; Data data; DataWidth width; } Proc2DMemReq deriving(Bits, Eq);
typedef struct { Data data; Bool error; } DMem2ProcResp deriving(Bits, Eq);
typedef struct { Instruction instr; Bool error; } IMem2ProcResp deriving(Bits, Eq);

//interface declaration
//processor interface:
    //Interface to Instruction Memory
    //    Memory gets an address from proccessor - Value method
    //    Memory puts back a value to the processor - An Action method
    //
    //Interface to Data Memory
    //    Memory gets a read/write request, along with an address,     - value method
    //    the data is valid only for write requests
    //    Memory puts back the data in the processor in case of read   - action method

interface ValueGet#(type req_type);
    method req_type get();
endinterface: ValueGet

interface ClientValueGet#(type req_type, type resp_type);
    interface ValueGet#(req_type) request;
    interface Put#(resp_type) response;
endinterface: ClientValueGet

interface ServerValueGet#(type req_type, type resp_type);
    interface ValueGet#(resp_type) response;
    interface Put#(req_type) request;
endinterface: ServerValueGet

interface ProcessorIFC;
    method Bool isInvalidInsr();
    method Bool isAluError();
    method Bool isIMemError();
    method Bool isDMemError();
    method Bool isProcStalled();
    interface ClientValueGet#(Address, IMem2ProcResp) proc2IMemIFC;
    interface ClientValueGet#(Proc2DMemReq, DMem2ProcResp) proc2DMemIFC;
endinterface: ProcessorIFC

endpackage: global_definations
