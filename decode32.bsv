`include "global_parameters"
package decode32;

//import packages
import Assert :: *;
import RegFile :: *;
import Vector :: *;
import global_definations :: *;

//constants declaration
//Declare Opcode -- move them to a different file later
//RV32I
Bit#(7) opLUI       = 7'b0110111;   //Utype - Load Upper Immediate
Bit#(7) opAUIPC     = 7'b0010111;   //Utype - Add Upper Immediate with PC
Bit#(7) opJAL       = 7'b1101111;   //JType - Jump and Link
Bit#(7) opJALR      = 7'b1100111;   //IType - Jump and Link Register
Bit#(7) opBranch    = 7'b1100111;   //BType - BEQ, BNE, BLT, BGE, BLTU, BGEU
Bit#(7) opLoad      = 7'b0000011;   //IType - LB, LH, LW, LBU, LHU
Bit#(7) opStore     = 7'b0100011;   //SType - SB, SH, SW
Bit#(7) opAluImm    = 7'b0010011;   //IType - ADDI, SLTI, SLTIU, XORI, ORI, ANDI, SLLI, SRLI, SRAI
Bit#(7) opAluR      = 7'b0110011;   //RType - ADD, SUB, SLL, SLT, SLTU, XOR, OR, AND, SRL, SRA
//RV32C
Bit#(2) c0 = 2'b00;
Bit#(2) c1 = 2'b01;
Bit#(2) c2 = 2'b10;
Bit#(2) c3 = 2'b11;

//Decode  function for 32bit integer instructions - RV32I
function ActionValue#(DecodeFnOut) decode_instruction(Instruction instruction, Vector#(`NumRegs, Reg#(Data)) regfile_v, Address instr_pc, RegIndex alu_reg, RegIndex mem_reg, RegIndex regwb_reg);
    actionvalue
        let opcode = instruction[6:0];
        Bool invalid_instr = False;
        DecodedInstruction decoded_instr;
        DecodeFnOut result;
        RegIndex rs1 = 0;
        RegIndex rs2 = 0;

        //Decode the instruction
        case (opcode)
            opLUI:
            begin
                //Load immediate to rd

                RegIndex rd = instruction[11:7];
                Data imm = {instruction[31:12], 12'b0};
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Invalid,
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Valid RegWrOp {operand:Imm, writereg:rd},
                                                       wrdata_or_memaddr: tagged WrData imm,
                                                       pcchangeop:tagged Invalid
                                                   };
`ifdef Simulation
                $display("Decode: LUI: rd = %d, imm = %d", rd, imm);
`endif
            end

            opAUIPC:
            begin
                //Add immediate to pc and store in rd
                //Assuming the immediate to be signed

                RegIndex rd = instruction[11:7];
                Data imm = {instruction[31:12], 12'b0};
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Pc instr_pc, operand2:tagged Indata imm},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage - wrdata
                                                       pcchangeop:tagged Invalid
                                                   };
`ifdef Simulation
                $display("Decode: AUIPC: rd = %d, imm = %d, pc = %d", rd, imm, instr_pc);
`endif
            end

            opJAL:
            begin
                //Add immediate to pc and jump there. Copy pc+4 to rd

                RegIndex rd = instruction[11:7];
                Data imm = signExtend({instruction[31], instruction[19:12], instruction[20], instruction[30:21],1'b0});
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Pc instr_pc, operand2:tagged Indata 32'h00000004},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage - wrdata
                                                       pcchangeop:tagged Valid PcChangeOp {operation:Jal, baseaddress:instr_pc, offset:imm}
                                                   };
`ifdef Simulation
                $display("Decode: JAL: rd = %d, imm = %d, pc = %d", rd, imm, instr_pc);
`endif
            end

            opJALR:
            begin
                //Replace pc with (rs1 + imm) and jum there. Copy pc+4 to rd. Note last bit of rs1 + imm should be set to 0

                RegIndex rd = instruction[11:7];
                rs1 = instruction[19:15];
                Data imm = signExtend(instruction[31:20]);
                Data regdata1 = regfile_v[rs1];
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Pc instr_pc, operand2:tagged Indata 32'h00000004},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                       wrdata_or_memaddr: tagged Invalid,     //Filled during alu stage - wrdata
                                                       pcchangeop:tagged Valid PcChangeOp {operation:Jal, baseaddress:unpack(pack(regdata1)), offset:imm}
                                                   };
`ifdef Simulation
                $display("Decode: JALR: rd = %d, rs1 = %d, imm = %d, pc = %d, [rs1] = %d", rd, rs1, imm, instr_pc, regdata1);
`endif
            end

            opBranch:
            begin
                //Compute pc + imm. Do the comparision operation and if success, replace pc.

                rs1 = instruction[19:15];
                rs2 = instruction[24:20];
                Data imm = signExtend({instruction[31], instruction[7], instruction[30:25], instruction[11:8], 1'b0});
                AluOperation aluop;
                Data regdata1 = regfile_v[rs1];
                Data regdata2 = regfile_v[rs2];
                case (instruction[14:12])
                    3'b000: aluop = Eq;
                    3'b001: aluop = NEq;
                    3'b100: aluop = Lt;
                    3'b101: aluop = Ge;
                    3'b110: aluop = LtU;
                    3'b111: aluop = GeU;
                    default: begin invalid_instr = True; aluop = Eq; end
                endcase
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:aluop, operand1:tagged Indata regdata1, operand2:tagged Indata regdata2},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Invalid,
                                                       wrdata_or_memaddr: tagged Invalid,
                                                       pcchangeop:tagged Valid PcChangeOp {operation:Branch, baseaddress:instr_pc, offset:imm}
                                                   };

`ifdef Simulation
                $display("Decode: Branch %s: rs1 = %d, rs2 = %d, imm = %d, pc = %d, [rs1] = %d, [rs2] = %d",
                         case(instruction[14:12]) 3'b000:"Eq"; 3'b001:"NEq"; 3'b100:"Lt"; 3'b101:"Ge"; 3'b110:"LtU"; 3'b111:"GeU"; endcase,
                         rs1, rs2, imm, instr_pc, regdata1, regdata2);
`endif
            end

            opLoad:
            begin
                //Compute rs1 + imm and load from this memory location to rd.

                DataWidth dw;
                DataSign ds;
                RegIndex rd = instruction[11:7];
                rs1 = instruction[19:15];
                Data imm = signExtend(instruction[31:20]);
                Data regdata1 = regfile_v[rs1];
                case (instruction[14:12])
                    3'b000: begin dw = B; ds = S; end
                    3'b001: begin dw = H; ds = S; end
                    3'b010: begin dw = W; ds = S; end
                    3'b100: begin dw = B; ds = U; end
                    3'b101: begin dw = H; ds = U; end
                    default: begin invalid_instr = True; dw = W; ds = S; end
                endcase
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                       memop:tagged Valid MemOp {operation:Load, datawidth:dw, datasign:ds ,storedata: 0},
                                                       regwrop:tagged Valid RegWrOp {operand:Mem, writereg:rd},
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- memaddr
                                                       pcchangeop:tagged Invalid
                                                   };
`ifdef Simulation
                $display("Decode: Load %s: rd = %d, rs1 = %d, imm = %d, [rs1] = %d",
                         case(instruction[14:12]) 3'b000:"BS"; 3'b001:"HS"; 3'b010:"WS"; 3'b100:"BU"; 3'b101:"HU"; endcase,
                         rd, rs1, imm, regdata1);
`endif
            end

            opStore:
            begin
                //Compute rs1 + imm and store rs2 into the memory address rs1+imm

                DataWidth dw;
                rs1 = instruction[19:15];
                rs2 = instruction[24:20];
                Data imm = signExtend({instruction[31:25], instruction[11:7]});
                Data regdata1 = regfile_v[rs1];
                Data regdata2 = regfile_v[rs2];
                case (instruction[14:12])
                    3'b000: dw = B;
                    3'b001: dw = H;
                    3'b010: dw = W;
                    default: begin invalid_instr = True; dw = W; end
                endcase
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                       memop:tagged Valid MemOp {operation:Store, datawidth:dw, datasign:S , storedata: regdata2},
                                                       regwrop:tagged Invalid,
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- memaddr
                                                       pcchangeop:tagged Invalid
                                                   };
`ifdef Simulation
                $display("Decode: Store %s: rs1 = %d, rs2 = %d, imm = %d, [rs1] = %d [rs2] = %d",
                         case(instruction[14:12]) 3'b000:"B"; 3'b001:"H"; 3'b010:"W"; endcase,
                         rs1, rs2, imm, regdata1, regdata2);
`endif
            end

            opAluImm:
            begin
                RegIndex rd = instruction[11:7];
                rs1 = instruction[19:15];
                Data imm = signExtend(instruction[31:20]);
                AluOperation aluop = Add; //Dummy
                Data regdata1 = regfile_v[rs1];
                case (instruction[14:12])
                    3'b000: aluop = Add;
                    3'b010: aluop = Lt;
                    3'b011: aluop = LtU;
                    3'b100: aluop = Xor;
                    3'b110: aluop = Or;
                    3'b111: aluop = And;

                    3'b001:
                    begin
                        if (instruction[31:25] != 7'b0)
                            invalid_instr = True;
                        imm = zeroExtend(instruction[24:20]);
                        aluop = LLS;
                    end

                    3'b101:
                    begin
                        imm = zeroExtend(instruction[24:20]);
                        case (instruction[31:25])
                            7'b0000000: aluop = LRS;
                            7'b0100000: aluop = ARS;
                            default: invalid_instr = True;
                        endcase
                    end
                    default: invalid_instr = True;
                endcase
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:aluop, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                       pcchangeop:tagged Invalid
                                                   };
`ifdef Simulation
                $display("Decode: AluI %s: rd = %d, rs1 = %d, imm = %d, [rs1] = %d",
                         case(instruction[14:12]) 3'b000:"Add"; 3'b010:"Lt"; 3'b011:"LtU"; 3'b100:"Xor"; 3'b110:"Or";
                                                  3'b111:"And"; 3'b001:"LLS"; 3'b101:case(instruction[31:25]) 7'b0000000:"LRS"; 7'b0100000:"ARS"; endcase
                         endcase, rd, rs1, imm, regdata1);
`endif
            end

            opAluR:
            begin
                RegIndex rd = instruction[11:7];
                rs1 = instruction[19:15];
                rs2 = instruction[24:20];
                AluOperation aluop = Add;  //Dummy
                Data regdata1 = regfile_v[rs1];
                Data regdata2 = regfile_v[rs2];
                case (instruction[14:12])
                    3'b000:
                    begin
                    //Add or Sub
                        if (instruction[31:25] == 7'b0)
                            aluop = Add;
                        else if (instruction[31:25] == 7'b0100000)
                            aluop = Sub;
                        else
                            invalid_instr = True;
                    end

                     3'b001:
                    begin
                    //Logical left shift
                        if (instruction[31:25] != 7'b0)
                            invalid_instr = True;
                        aluop = LLS;
                    end

                    3'b010:
                    begin
                    //Set less than
                        if (instruction[31:25] != 7'b0)
                            invalid_instr = True;
                        aluop = Lt;
                    end

                    3'b011:
                    begin
                    //Set less than unsigned
                        if (instruction[31:25] != 7'b0)
                            invalid_instr = True;
                        aluop = LtU;
                    end

                    3'b100:
                    begin
                    //Xor
                        if (instruction[31:25] != 7'b0)
                            invalid_instr = True;
                        aluop = Xor;
                    end

                    3'b101:
                    //Logical right/arithmetic shift
                    begin
                        if (instruction[31:25] == 7'b0)
                            aluop = LRS;
                        else if (instruction[31:25] == 7'b0100000)
                            aluop = ARS;
                        else
                            invalid_instr = True;
                    end

                    3'b110:
                    //Or
                    begin
                        if (instruction[31:25] != 7'b0)
                            invalid_instr = True;
                        aluop = Or;
                    end

                    3'b111:
                    begin
                    //And
                        if (instruction[31:25] != 7'b0)
                            invalid_instr = True;
                        aluop = And;
                    end

                    default:
                    begin
                    invalid_instr = True;
                    aluop = Add;  //Dummy
                    end
                endcase
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:aluop, operand1:tagged Indata regdata1, operand2:tagged Indata regdata2},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                       pcchangeop:tagged Invalid
                                                   };
`ifdef Simulation
                $display("Decode: AluR %s: rd = %d, rs1 = %d, rs2 = %d, [rs1] = %d, [rs2] = %d",
                         case(instruction[14:12]) 3'b000:case(instruction[31:25]) 7'b0:"Add"; 7'b0100000: "Sub"; endcase 3'b010:"Lt"; 3'b011:"LtU"; 3'b100:"Xor";
                                                  3'b110:"Or"; 3'b111:"And"; 3'b001:"LLS"; 3'b101:case(instruction[31:25]) 7'b0000000:"LRS"; 7'b0100000:"ARS"; endcase
                         endcase, rd, rs1, rs2, regdata1, regdata2);
`endif
            end
            default:
            begin
                invalid_instr = True;
                decoded_instr = DecodedInstruction {aluop:tagged Invalid, memop:tagged Invalid, regwrop:tagged Invalid, wrdata_or_memaddr:tagged Invalid, pcchangeop:tagged Invalid };
            end
        endcase

        //detect data hazards
        if (((0 != rs1) && (rs1 == alu_reg)) || ((0 != rs2) && (rs2 == alu_reg))) result.data_hazard = True;
        else if (((0 != rs1) && (rs1 == mem_reg)) || ((0 != rs2) && (rs2 == mem_reg))) result.data_hazard = True;
        else if (((0 != rs1) && (rs1 == regwb_reg)) || ((0 != rs2) && (rs2 == regwb_reg))) result.data_hazard = True;
        else result.data_hazard = False;
`ifdef Simulation
        if (((0 != rs1) && (rs1 == alu_reg)) || ((0 != rs2) && (rs2 == alu_reg))) $display("Decode: Data hazard with alu phase - reg %d", alu_reg);
        else if (((0 != rs1) && (rs1 == mem_reg)) || ((0 != rs2) && (rs2 == mem_reg))) $display("Decode: Data hazard with mem phase - reg %d", mem_reg);
        else if (((0 != rs1) && (rs1 == regwb_reg)) || ((0 != rs2) && (rs2 == regwb_reg))) $display("Decode: Data hazard with wb phase - reg %d", regwb_reg);
        $display("Decode: cur_instruction = 0x%x, instr_pc = %d, invalid_instr = %d, alu_reg = %d, mem_reg = %d, wb_reg= %d", instruction, instr_pc, invalid_instr, alu_reg, mem_reg, regwb_reg);
`endif

        if (invalid_instr)
            result.decoded_instr = tagged Invalid;
        else
            result.decoded_instr = tagged Valid decoded_instr;
        return result;
    endactionvalue
endfunction: decode_instruction




//Decode  function for compressed instructions - RV32C
function ActionValue#(DecodeFnOut) decode_cinstruction (
    CInstruction instruction,
    Vector#(`NumRegs, Reg#(Data)) regfile_v,
    Address instr_pc,
    RegIndex alu_reg,
    RegIndex mem_reg,
    RegIndex regwb_reg
    );

    actionvalue
        Bool invalid_instr = False;
        DecodedInstruction decoded_instr = DecodedInstruction {aluop:tagged Invalid, memop:tagged Invalid, regwrop:tagged Invalid, wrdata_or_memaddr:tagged Invalid, pcchangeop:tagged Invalid };
        DecodeFnOut result;
        RegIndex rd = 0;
        RegIndex rs1 = 0;
        RegIndex rs2 = 0;
        Data imm = 0;

`ifdef Compress
        //Decode the instruction
        if(c0 == instruction[1:0]) begin    //Possible instructions: C.LW; C.SW;
            if (3'b010 == instruction[15:13]) begin    //Load
                //Compute rs1 + imm and load from this memory location to rd.
                rd = unpack({01, instruction[4:2]});
                rs1 = unpack({01, instruction[9:7]});
                imm = zeroExtend({instruction[5], instruction[12:10], instruction[6], 2'b00});
                Data regdata1 = regfile_v[rs1];
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                       memop:tagged Valid MemOp {operation:Load, datawidth:W, datasign:S ,storedata: 0},
                                                       regwrop:tagged Valid RegWrOp {operand:Mem, writereg:rd},
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- memaddr
                                                       pcchangeop:tagged Invalid
                                                   };
`ifdef Simulation
                $display("Decode-Comp: Load: rd = %d, rs1 = %d, imm = %d, [rs1] = %d", rd, rs1, imm, regdata1);
`endif
            end else if (3'b110 == instruction[15:13]) begin   //Store
                //Compute rs1 + imm and store rs2 into the memory address rs1+imm
                rs1 = unpack({01, instruction[9:7]});
                rs2 = unpack({01, instruction[4:2]});
                imm = zeroExtend({instruction[5], instruction[12:10], instruction[6], 2'b00});
                Data regdata1 = regfile_v[rs1];
                Data regdata2 = regfile_v[rs2];
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                       memop:tagged Valid MemOp {operation:Store, datawidth:W, datasign:S , storedata: regdata2},
                                                       regwrop:tagged Invalid,
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- memaddr
                                                       pcchangeop:tagged Invalid
                                                   };
`ifdef Simulation
                $display("Decode-Comp: Store : rs1 = %d, rs2 = %d, imm = %d, [rs1] = %d [rs2] = %d", rs1, rs2, imm, regdata1, regdata2);
`endif
            end else invalid_instr = True;
            //end - c0 - //Possible instructions: C.LW; C.SW;
        end else if (c1 == instruction[1:0]) begin   //Possible instructions: C.J; C.JAL; C.BEQZ; C.BNEZ; C.LI; C.LUI; C.ADDI; C.ADDI16SP; C.SRLI; C.SRAI; C.ANDI; C.AND; C.OR; C.XOR; C.SUB; C.NOP
            if (3'b000 == instruction[15:13]) begin //C.NOP - expected is addi x0, x0, 0. But this implementation is better.
                if (instruction[12:2] != 0) invalid_instr = True;
                decoded_instr = DecodedInstruction {aluop:tagged Invalid, memop:tagged Invalid, regwrop:tagged Invalid, wrdata_or_memaddr:tagged Invalid, pcchangeop:tagged Invalid };
`ifdef Simulation
                $display("Decode-Comp: NOP pc = %d", instr_pc);
`endif
            end else if (3'b001 == instruction[15:13]) begin //C.J
                //Add immediate to pc and jump there.
                rd = 0;
                imm = signExtend({instruction[12], instruction[8], instruction[10:9], instruction[6], instruction[7], instruction[2], instruction[11], instruction[5:3], 1'b0});
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Pc instr_pc, operand2:tagged Indata 32'h00000002},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage - wrdata
                                                       pcchangeop:tagged Valid PcChangeOp {operation:Jal, baseaddress:instr_pc, offset:imm}
                                                   };
`ifdef Simulation
                $display("Decode-Comp: JAL: rd = %d, imm = %d, pc = %d", rd, imm, instr_pc);
`endif
            end else if (3'b101 == instruction[15:13]) begin //C.JAL
                //Add immediate to pc and jump there. Copy pc+2 to rd
                rd = 1;
                imm = signExtend({instruction[12], instruction[8], instruction[10:9], instruction[6], instruction[7], instruction[2], instruction[11], instruction[5:3], 1'b0});
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Pc instr_pc, operand2:tagged Indata 32'h00000002},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage - wrdata
                                                       pcchangeop:tagged Valid PcChangeOp {operation:Jal, baseaddress:instr_pc, offset:imm}
                                                   };
`ifdef Simulation
                $display("Decode-Comp: JAL: rd = %d, imm = %d, pc = %d", rd, imm, instr_pc);
`endif
            end else if (3'b110 == instruction[15:13]) begin //C.BEQZ
                //Compute pc + imm. Do the comparision operation and if success, replace pc.
                rs1 = unpack({2'b01,instruction[9:7]});
                rs2 = 0;
                imm = signExtend({instruction[12], instruction[6:5], instruction[2], instruction[11:10], instruction[4:3], 1'b0});
                Data regdata1 = regfile_v[rs1];
                Data regdata2 = 0;
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Eq, operand1:tagged Indata regdata1, operand2:tagged Indata regdata2},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Invalid,
                                                       wrdata_or_memaddr: tagged Invalid,
                                                       pcchangeop:tagged Valid PcChangeOp {operation:Branch, baseaddress:instr_pc, offset:imm}
                                                   };

`ifdef Simulation
                $display("Decode-Comp: Branch Eq: rs1 = %d, rs2 = %d, imm = %d, pc = %d, [rs1] = %d, [rs2] = %d", rs1, rs2, imm, instr_pc, regdata1, regdata2);
`endif
            end else if (3'b111 == instruction[15:13]) begin //C.BNEZ
                //Compute pc + imm. Do the comparision operation and if success, replace pc.
                rs1 = unpack({2'b01,instruction[9:7]});
                rs2 = 0;
                imm = signExtend({instruction[12], instruction[6:5], instruction[2], instruction[11:10], instruction[4:3], 1'b0});
                Data regdata1 = regfile_v[rs1];
                Data regdata2 = 0;
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:NEq, operand1:tagged Indata regdata1, operand2:tagged Indata regdata2},
                                                       memop:tagged Invalid,
                                                       regwrop:tagged Invalid,
                                                       wrdata_or_memaddr: tagged Invalid,
                                                       pcchangeop:tagged Valid PcChangeOp {operation:Branch, baseaddress:instr_pc, offset:imm}
                                                   };

`ifdef Simulation
                $display("Decode-Comp: Branch NEq: rs1 = %d, rs2 = %d, imm = %d, pc = %d, [rs1] = %d, [rs2] = %d", rs1, rs2, imm, instr_pc, regdata1, regdata2);
`endif
            end else if (3'b010 == instruction[15:13]) begin //C.LI - load rd with imm
                if (instruction[11:7] != 0) begin
                    rd = instruction[11:7];
                    rs1 = 0;
                    imm = signExtend({instruction[12], instruction[6:2]});
                    Data regdata1 = 0;
                    decoded_instr = DecodedInstruction {
                                                           aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                           memop:tagged Invalid,
                                                           regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                           wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                           pcchangeop:tagged Invalid
                                                        };
`ifdef Simulation
                    $display("Decode-Comp: AluI Add: rd = %d, rs1 = %d, imm = %d, [rs1] = %d", rd, rs1, imm, regdata1);
`endif
                end else invalid_instr = True;
            end else if (3'b011 == instruction[15:13]) begin //C.LUI - load rd with imm
                if (({instruction[12], instruction[6:2]} != 0) && (instruction[11:7] != 0) && (instruction[11:7] != 2)) begin
                    rd = instruction[11:7];
                    imm = signExtend({instruction[12], instruction[6:2], 12'b0});
                    decoded_instr = DecodedInstruction {
                                                           aluop:tagged Invalid,
                                                           memop:tagged Invalid,
                                                           regwrop:tagged Valid RegWrOp {operand:Imm, writereg:rd},
                                                           wrdata_or_memaddr: tagged WrData imm,
                                                           pcchangeop:tagged Invalid
                                                       };
`ifdef Simulation
                    $display("Decode-Comp: LUI: rd = %d, imm = %d", rd, imm);
`endif
                end else invalid_instr = True;
            end else if (3'b000 == instruction[15:13]) begin //C.ADDI
                if (({instruction[12], instruction[6:2]} != 0) && (instruction[11:7] != 0)) begin
                    rd = instruction[11:7];
                    rs1 = instruction[11:7];
                    imm = signExtend({instruction[12], instruction[6:2]});
                    Data regdata1 = regfile_v[rs1];
                    decoded_instr = DecodedInstruction {
                                                           aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                           memop:tagged Invalid,
                                                           regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                           wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                           pcchangeop:tagged Invalid
                                                       };
`ifdef Simulation
                    $display("Decode-Comp: AluI Add: rd = %d, rs1 = %d, imm = %d, [rs1] = %d", rd, rs1, imm, regdata1);
`endif
                end else invalid_instr = True;
            end else if (3'b011 == instruction[15:13]) begin //C.ADDI16SP
                if (({instruction[12], instruction[6:2]} != 0) && (instruction[11:7] == 2)) begin
                    rd = 2;
                    rs1 = 2;
                    imm = signExtend({instruction[12], instruction[4:3], instruction[5], instruction[2], instruction[6], 5'b00000});
                    Data regdata1 = regfile_v[rs1];
                    decoded_instr = DecodedInstruction {
                                                           aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                           memop:tagged Invalid,
                                                           regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                           wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                           pcchangeop:tagged Invalid
                                                       };
`ifdef Simulation
                    $display("Decode-Comp: AluI Add: rd = %d, rs1 = %d, imm = %d, [rs1] = %d", rd, rs1, imm, regdata1);
`endif
                end else invalid_instr = True;
            end else if (3'b100 == instruction[15:13]) begin //C.SRLI; C.SRAI; C.ANDI; C.AND; C.OR; C.XOR; C.SUB;
                case(instruction[11:10])
                    2'b00: //C.SRLI
                    if ((instruction[12] == 0) && (instruction[6:2] != 0)) begin
                        rd = unpack({2'b01,instruction[9:7]});
                        rs1 = unpack({2'b01,instruction[9:7]});
                        imm = zeroExtend(instruction[6:2]);
                        Data regdata1 = regfile_v[rs1];
                        decoded_instr = DecodedInstruction {
                                                               aluop:tagged Valid AluOp {operation:LRS, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                               memop:tagged Invalid,
                                                               regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                               wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                               pcchangeop:tagged Invalid
                                                           };
`ifdef Simulation
                        $display("Decode-Comp: AluI LRS: rd = %d, rs1 = %d, imm = %d, [rs1] = %d",
                                                                 rd, rs1, imm, regdata1);
`endif
                    end else invalid_instr = True;

                    2'b01: //C.SRAI
                    if ((instruction[12] == 0) && (instruction[6:2] != 0)) begin
                        rd = unpack({2'b01,instruction[9:7]});
                        rs1 = unpack({2'b01,instruction[9:7]});
                        imm = zeroExtend(instruction[6:2]);
                        Data regdata1 = regfile_v[rs1];
                        decoded_instr = DecodedInstruction {
                                                               aluop:tagged Valid AluOp {operation:ARS, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                               memop:tagged Invalid,
                                                               regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                               wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                               pcchangeop:tagged Invalid
                                                           };
`ifdef Simulation
                        $display("Decode-Comp: AluI ARS: rd = %d, rs1 = %d, imm = %d, [rs1] = %d", rd, rs1, imm, regdata1);
`endif
                    end else invalid_instr = True;

                    2'b10: //C.ANDI
                    begin
                        rd = unpack({2'b01,instruction[9:7]});
                        rs1 = unpack({2'b01,instruction[9:7]});
                        imm = signExtend({instruction[12], instruction[6:2]});
                        Data regdata1 = regfile_v[rs1];
                        decoded_instr = DecodedInstruction {
                                                               aluop:tagged Valid AluOp {operation:And, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                               memop:tagged Invalid,
                                                               regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                               wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                               pcchangeop:tagged Invalid
                                                           };
`ifdef Simulation
                        $display("Decode-Comp: AluI And: rd = %d, rs1 = %d, imm = %d, [rs1] = %d", rd, rs1, imm, regdata1);
`endif
                    end

                    2'b11: //C.AND; C.OR; C.XOR; C.SUB;
                    if (instruction[12] == 0) begin
                        rd = unpack({2'b01,instruction[9:7]});
                        rs1 = unpack({2'b01,instruction[9:7]});
                        rs2 = unpack({2'b01,instruction[4:2]});
                        AluOperation aluop = Add;  //dummy
                        Data regdata1 = regfile_v[rs1];
                        Data regdata2 = regfile_v[rs2];
                        case(instruction[6:5])
                            2'b11: aluop = And;//C.AND
                            2'b10: aluop = Or;//C.OR
                            2'b01: aluop = Xor;//C.XOR
                            2'b00: aluop = Sub;//C.SUB
                        endcase
                        decoded_instr = DecodedInstruction {
                                                               aluop:tagged Valid AluOp {operation:aluop, operand1:tagged Indata regdata1, operand2:tagged Indata regdata2},
                                                               memop:tagged Invalid,
                                                               regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                               wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                               pcchangeop:tagged Invalid
                                                           };
`ifdef Simulation
                        $display("Decode-Comp: AluR %s: rd = %d, rs1 = %d, rs2 = %d, [rs1] = %d, [rs2] = %d",
                                 case(aluop) And:"And"; Or:"Or"; Xor:"Xor"; Sub:"Sub"; endcase,
                                 rd, rs1, rs2, regdata1, regdata2);
`endif
                    end else invalid_instr = True;
                endcase
            end else invalid_instr = True;
            //end - c1 - //Possible instructions: C.J; C.JAL; C.BEQZ; C.BNEZ; C.LI; C.LUI; C.ADDI; C.ADDI16SP; C.SRLI; C.SRAI; C.ANDI; C.AND; C.OR; C.XOR; C.SUB; C.NOP
        end else if (c2 == instruction[1:0]) begin    //Possible instructions: C.LWSP; C.SWSP; C.JR; C.JALR; C.SLLI; C.MV; C.ADD;
            if (3'b010 == instruction[15:13]) begin //C.LWSP - load
                //Compute rs1 + imm and load from this memory location to rd.
                rd = instruction[11:7];
                rs1 = 2;    //rs1 = x2
                imm = zeroExtend({instruction[3:2], instruction[12], instruction[6:4], 2'b00});
                Data regdata1 = regfile_v[rs1];
                if (rd != 0) begin
                    decoded_instr = DecodedInstruction {
                                                           aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                           memop:tagged Valid MemOp {operation:Load, datawidth:W, datasign:S ,storedata: 0},
                                                           regwrop:tagged Valid RegWrOp {operand:Mem, writereg:rd},
                                                           wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- memaddr
                                                           pcchangeop:tagged Invalid
                                                       };
`ifdef Simulation
                    $display("Decode-Comp: Load: rd = %d, rs1 = %d, imm = %d, [rs1] = %d", rd, rs1, imm, regdata1);
`endif
                end else invalid_instr = True;
            end else if (3'b110 == instruction[15:13]) begin //C.SWSP - store
                //Compute rs1 + imm and store rs2 into the memory address rs1+imm
                rs1 = 2;    //rs1 = x2;
                rs2 = instruction[6:2];
                imm = zeroExtend({instruction[8:7], instruction[12:9], 2'b00});
                Data regdata1 = regfile_v[rs1];
                Data regdata2 = regfile_v[rs2];
                decoded_instr = DecodedInstruction {
                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                       memop:tagged Valid MemOp {operation:Store, datawidth:W, datasign:S , storedata: regdata2},
                                                       regwrop:tagged Invalid,
                                                       wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- memaddr
                                                       pcchangeop:tagged Invalid
                                                   };
`ifdef Simulation
                $display("Decode-Comp: Store : rs1 = %d, rs2 = %d, imm = %d, [rs1] = %d [rs2] = %d", rs1, rs2, imm, regdata1, regdata2);
`endif
            end else if (3'b000 == instruction[15:13]) begin //C.SLLI - Logical left shift
                rd = instruction[11:7];
                rs1 = instruction[11:7];
                imm = zeroExtend(instruction[6:2]);
                Data regdata1 = regfile_v[rs1];
                if ((imm != 0) && (rd != 0) && (instruction[12] == 0)) begin
                    decoded_instr = DecodedInstruction {
                                                           aluop:tagged Valid AluOp {operation:LLS, operand1:tagged Indata regdata1, operand2:tagged Indata imm},
                                                           memop:tagged Invalid,
                                                           regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                           wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                           pcchangeop:tagged Invalid
                                                       };
`ifdef Simulation
                    $display("Decode-Comp: AluI LLS: rd = %d, rs1 = %d, imm = %d, [rs1] = %d", rd, rs1, imm, regdata1);
`endif
                end else invalid_instr = True;
            end else if (3'b100 == instruction[15:13]) begin //C.JR; C.JALR; C.MV; C.ADD;
                if (instruction[11:7] != 0) begin
                    case(instruction[6:2])
                        5'b00000:    //C.JR; C.JALR;
                        case(instruction[12])
                            1'b0:    //C.JR
                            begin
                                //Replace pc with rs1 and jump there.
                                rd = 0;
                                rs1 = instruction[11:7];
                                imm = 0;
                                Data regdata1 = regfile_v[rs1];
                                decoded_instr = DecodedInstruction {
                                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Pc instr_pc, operand2:tagged Indata 32'h00000002},
                                                                       memop:tagged Invalid,
                                                                       regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                                       wrdata_or_memaddr: tagged Invalid,     //Filled during alu stage - wrdata
                                                                       pcchangeop:tagged Valid PcChangeOp {operation:Jal, baseaddress:unpack(pack(regdata1)), offset:imm}
                                                                   };
`ifdef Simulation
                                $display("Decode-Comp: JALR: rd = %d, rs1 = %d, imm = %d, pc = %d, [rs1] = %d", rd, rs1, imm, instr_pc, regdata1);
`endif
                            end  //C.JR

                            1'b1:    //C.JALR
                            begin
                                //Replace pc with rs1 and jump there. x1 = pc + 2
                                rd = 1;    //rd = x1
                                rs1 = instruction[11:7];
                                imm = 0;
                                Data regdata1 = regfile_v[rs1];
                                decoded_instr = DecodedInstruction {
                                                                       aluop:tagged Valid AluOp {operation:Add, operand1:tagged Pc instr_pc, operand2:tagged Indata 32'h00000002},
                                                                       memop:tagged Invalid,
                                                                       regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                                       wrdata_or_memaddr: tagged Invalid,     //Filled during alu stage - wrdata
                                                                       pcchangeop:tagged Valid PcChangeOp {operation:Jal, baseaddress:unpack(pack(regdata1)), offset:imm}
                                                                   };
`ifdef Simulation
                                $display("Decode-Comp: JALR: rd = %d, rs1 = %d, imm = %d, pc = %d, [rs1] = %d", rd, rs1, imm, instr_pc, regdata1);
`endif
                            end  //C.JALR
                        endcase
                        //end - //C.JR; C.JALR;

                        default:    //C.MV; C.ADD;
                        if (instruction[6:2] != 0)
                            case(instruction[12])
                                1'b0:    //C.MV - move rs2 into rd i.e. rd = rs2
                                begin
                                    rd = instruction[11:7];
                                    rs1 = 0;
                                    rs2 = instruction[6:2];
                                    Data regdata1 = 0;
                                    Data regdata2 = regfile_v[rs2];
                                    decoded_instr = DecodedInstruction {
                                                                           aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata regdata2},
                                                                           memop:tagged Invalid,
                                                                           regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                                           wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                                           pcchangeop:tagged Invalid
                                                                       };
`ifdef Simulation
                                    $display("Decode-Comp: AluR Add: rd = %d, rs1 = %d, rs2 = %d, [rs1] = %d, [rs2] = %d", rd, rs1, rs2, regdata1, regdata2);
`endif
                                end  //C.MV

                                1'b1:    //C.ADD - rd = rd + rs2
                                begin
                                    rd = instruction[11:7];
                                    rs1 = instruction[11:7];
                                    rs2 = instruction[6:2];
                                    Data regdata1 = regfile_v[rs1];
                                    Data regdata2 = regfile_v[rs2];
                                    decoded_instr = DecodedInstruction {
                                                                           aluop:tagged Valid AluOp {operation:Add, operand1:tagged Indata regdata1, operand2:tagged Indata regdata2},
                                                                           memop:tagged Invalid,
                                                                           regwrop:tagged Valid RegWrOp {operand:Alu, writereg:rd},
                                                                           wrdata_or_memaddr: tagged Invalid,    //Filled during alu stage -- wrdata
                                                                            pcchangeop:tagged Invalid
                                                                       };
`ifdef Simulation
                                    $display("Decode-Comp: AluR Add: rd = %d, rs1 = %d, rs2 = %d, [rs1] = %d, [rs2] = %d", rd, rs1, rs2, regdata1, regdata2);
`endif
                                end  //C.ADD
                            endcase
                        else invalid_instr = True;
                        //end - //C.JR; C.JALR;
                    endcase
                end else invalid_instr = True;
                //end - //C.JR; C.JALR; C.MV; C.ADD
            end else invalid_instr = True;
        //end - c2 - //Possible instructions: C.LWSP; C.SWSP; C.JR; C.JALR; C.SLLI; C.MV; C.ADD;

        end else invalid_instr = True;

        //Detect data hazards.
        if (((0 != rs1) && (rs1 == alu_reg)) || ((0 != rs2) && (rs2 == alu_reg))) result.data_hazard = True;
        else if (((0 != rs1) && (rs1 == mem_reg)) || ((0 != rs2) && (rs2 == mem_reg))) result.data_hazard = True;
        else if (((0 != rs1) && (rs1 == regwb_reg)) || ((0 != rs2) && (rs2 == regwb_reg))) result.data_hazard = True;
        else result.data_hazard = False;
`ifdef Simulation
        if (((0 != rs1) && (rs1 == alu_reg)) || ((0 != rs2) && (rs2 == alu_reg))) $display("Decode-Comp: Data hazard with alu phase - reg %d", alu_reg);
        else if (((0 != rs1) && (rs1 == mem_reg)) || ((0 != rs2) && (rs2 == mem_reg))) $display("Decode-Comp: Data hazard with mem phase - reg %d", mem_reg);
        else if (((0 != rs1) && (rs1 == regwb_reg)) || ((0 != rs2) && (rs2 == regwb_reg))) $display("Decode-Comp: Data hazard with wb phase - reg %d", regwb_reg);
        $display("Decode-Comp: cur_instruction = 0x%x, instr_pc = %d, invalid_instr = %d, alu_reg = %d, mem_reg = %d, wb_reg= %d", instruction, instr_pc, invalid_instr, alu_reg, mem_reg, regwb_reg);
`endif

`endif //ifdef Compress
`ifndef Compress
    invalid_instr <= True;
`endif

        if (invalid_instr)
            result.decoded_instr = tagged Invalid;
        else
            result.decoded_instr = tagged Valid decoded_instr;
        return result;

    endactionvalue
endfunction: decode_cinstruction

endpackage: decode32
