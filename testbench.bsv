`include "global_parameters"

`define Imem_file "./test/machine_code"
`define DataMemSz 128
`define InstrMemSz 1024

package testbench;

//import packages
import processor32 :: *;
import Assert :: *;
import RegFile :: *;
import DReg :: *;
import GetPut :: *;
import ClientServer :: *;
import Connectable :: *;    //is this needed ? It is needed in the testbench to connect interfaces.
import global_definations :: *;

//export variables

//define types

//interface declaration

//constants declaration

//function declaration

//module declaration
(*synthesize*)
module mkTestbench (Empty);

    //Constant declerations

    //Module instantiations
    Reg#(Data) clk_cycles <- mkReg(0);
    ProcessorIFC processor32 <- mkProcessor32(0); //pc starts with 0
    ServerValueGet#(Proc2DMemReq, DMem2ProcResp) datamem <- mkDataMem();
    ServerValueGet#(Address, IMem2ProcResp) instrmem <- mkInstrMem();

    //Function declarations

    //Connecting processor and memory
    mkConnection(instrmem.request.put, processor32.proc2IMemIFC.request.get);
    mkConnection(processor32.proc2IMemIFC.response.put, instrmem.response.get);
    mkConnection(datamem.request.put, processor32.proc2DMemIFC.request.get);
    mkConnection(processor32.proc2DMemIFC.response.put, datamem.response.get);

    //Rules
    rule count_cycles;
        clk_cycles <= clk_cycles + 1;
    endrule: count_cycles

    rule print;
        $display("-----------------------------------------------------------------------------");
        $display("Cycle: %d", clk_cycles);
    endrule: print

    //check error signals
    rule check_alu_error;
        dynamicAssert(False == processor32.isAluError, "Alu error occured");
    endrule: check_alu_error
    rule check_imem_error;
        dynamicAssert(False == processor32.isIMemError, "Instruction memory error occured");
    endrule: check_imem_error
    rule check_dmem_error;
        dynamicAssert(False == processor32.isDMemError, "Data memory error occured");
    endrule: check_dmem_error

    rule terminate (processor32.isInvalidInsr);
        $display("Terminating");
        $finish();
    endrule: terminate

    //Methods

    //Subinterfaces

endmodule: mkTestbench

(*synthesize*)
module mkDataMem (ServerValueGet#(Proc2DMemReq, DMem2ProcResp));
    RegFile#(Address, Data) datamem <- mkRegFile(0, fromInteger(valueof(`DataMemSz)-1));
    Reg#(Maybe#(Address)) request_addr <- mkDReg(tagged Invalid);
    Reg#(Bool) error <- mkDReg(False);

    interface ValueGet response;
        method DMem2ProcResp get() if (isValid(request_addr) || error);
            DMem2ProcResp resp;
            resp.data  = datamem.sub(fromMaybe(0,request_addr)>>2);
            resp.error = error;

            return resp;
        endmethod: get
    endinterface: response

    interface Put request;
        method Action put(Proc2DMemReq req);
            if (req.rw == Read) begin
                request_addr <= tagged Valid req.addr;
                //Check bounds and indicate any error
            end else if (req.rw == Write) begin
                Data cur = datamem.sub(req.addr>>2);
                case(req.width)
                    B: cur[7:0] = req.data[7:0];
                    H: cur[15:0] = req.data[15:0];
                    W: cur = req.data;
                    default: error <= True;    //In case of errors, leave data in memory intact
                endcase
                datamem.upd(req.addr>>2, cur); //takecare of data widths - B, H, W
                //Check bounds and indicate any error
            end else begin
                error <= True;
                dynamicAssert(False, "Unknown request sent to Data Memory");
            end
        endmethod: put
    endinterface: request

endmodule: mkDataMem

(*synthesize*)
module mkInstrMem (ServerValueGet#(Address, IMem2ProcResp));
    RegFile#(Address, Instruction) instrmem <- mkRegFileFullLoadBin(`Imem_file);
    Reg#(Maybe#(Address)) request_addr <- mkDReg(tagged Invalid);
    Reg#(Bool) error <- mkDReg(False);

    interface ValueGet response;
        method IMem2ProcResp get() if (isValid(request_addr) || error);
            IMem2ProcResp resp;
            if ((fromMaybe(0,request_addr))[1] == 1'b0)
                //32bit - aligned access
                resp.instr = instrmem.sub(fromMaybe(0,request_addr) >> 2);
            else begin
                //treat as 16bit - aligned access
                Instruction instr1 = instrmem.sub(fromMaybe(0,request_addr) >> 2);
                Instruction instr2 = instrmem.sub((fromMaybe(0,request_addr) >> 2) + 1);
                resp.instr = unpack({instr2[15:0], instr1[31:16]});
            end

            resp.error = error;
            return resp;
        endmethod: get
    endinterface: response

    interface Put request;
        method Action put(Address req);
            request_addr <= tagged Valid req;
            //Check bounds and indicate any error
        endmethod: put
    endinterface: request
endmodule: mkInstrMem

endpackage: testbench
