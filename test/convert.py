#!/usr/bin/python
# This script takes assembly RISCV instructions in the format mentioned below and
# converts them to the corresponding machine code in binary format

import sys
from bitstring import Bits

IN_FILE = "assembly_code"
OUT_FILE = "machine_code"

# Code for processing command line arguments
# Taken from https://gist.github.com/dideler/2395703
def getopts(argv):
        argv = argv[1:] #get rid of the script name in argument list
        if len(argv)%2 != 0:
                print("Need even number of arguments!")
                print("Correct format: ./convert.py -infile <> -outfile <>")
                return {}
        opts = {}  #Empty dictionary to store key-value pairs
        while argv:  #While there are arguments left to parse
                if argv[0][0] == '-':  #Found a "-name value" pair
                        opts[argv[0]] = argv[1]  #Add key and value to the dictionary
                argv = argv[2:]  #Remove the processed arguments
        return opts

myargs = getopts(sys.argv)
if '-infile' in myargs:
        IN_FILE = myargs['-infile']

if '-outfile' in myargs:
        OUT_FILE = myargs['-outfile']

# Supported Instructions:
# LUI	rd	imm
# AUIPC	rd	imm
# JAL	rd	imm
# JALR	rd	rs1	imm
# BEQ	rs1	rs2	imm
# BNE	rs1	rs2	imm
# BLT	rs1	rs2	imm
# BGE	rs1	rs2	imm
# BLTU	rs1	rs2	imm
# BGEU	rs1	rs2	imm
# LB	rd	rs1	imm
# LBU	rd	rs1	imm
# LH	rd	rs1	imm
# LHU	rd	rs1	imm
# LW	rd	rs1	imm	//load (rs1+imm) to rd
# SB	rs1	rs2	imm	//store rs2 to to (rs1+imm)
# SH	rs1	rs2	imm	//store rs2 to to (rs1+imm)
# SW	rs1	rs2	imm	//store rs2 to to (rs1+imm)
# ADDI	rd	rs1	imm
# SLTI	rd	rs1	imm
# SLTIU	rd	rs1	imm
# XORI	rd	rs1	imm
# ORI	rd	rs1	imm
# ANDI	rd	rs1	imm
# SLLI	rd	rs1	imm	//imm=shamt
# SRLI	rd	rs1	imm	//imm=shamt
# SRAI  rd	rs1	imm	//imm=shamt
# ADD	rd	rs1	rs2
# SUB	rd	rs1	rs2
# SLL	rd	rs1	rs2
# SLT	rd	rs1	rs2
# SLTU	rd	rs1	rs2
# XOR	rd	rs1	rs2
# SRL	rd	rs1	rs2
# SRA	rd	rs1	rs2
# OR	rd	rs1	rs2
# AND	rd	rs1	rs2

infile=open(IN_FILE, 'r')
outfile=open(OUT_FILE, 'w')
addr = 0;
terminator = Bits(int=int(-1), length=32)

lines=infile.readlines()
#lines = [line.rstrip('\n') for line in open('filename')]
for line in lines:
	line=line[:len(line)-1]	#Remove the '\n' from end of line
	instr=line.split('\t')

	if instr[0].lower() == 'LUI'.lower():
		# LUI	rd	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		imm = Bits(int=int(instr[2]), length=20).bin
		mach_code = imm + rd + "0110111" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'AUIPC'.lower():
		# AUIPC	rd	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		imm = Bits(int=int(instr[2]), length=20).bin
		mach_code = imm + rd + "0010111" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'JAL'.lower():
		# JAL	rd	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		imm = Bits(int=int(instr[2]), length=20).bin
		mach_code = imm[0] + imm[10:20] + imm[9] + imm[1:9] + rd + "1101111" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'JALR'.lower():
		# JALR	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "000" + rd + "1100111" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'BEQ'.lower():
		# BEQ	rs1	rs2	imm
		rs1 = Bits(int=int(instr[1][1:]), length=5).bin
		rs2 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm[0] + imm[2:8] + rs2 + rs1 + "000" + imm[8:13] + imm[1] + "1100011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'BNE'.lower():
		# BNE	rs1	rs2	imm
		rs1 = Bits(int=int(instr[1][1:]), length=5).bin
		rs2 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm[0] + imm[2:8] + rs2 + rs1 + "001" + imm[8:12] + imm[1] + "1100011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'BLT'.lower():
		# BLT	rs1	rs2	imm
		rs1 = Bits(int=int(instr[1][1:]), length=5).bin
		rs2 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm[0] + imm[2:8] + rs2 + rs1 + "100" + imm[8:12] + imm[1] + "1100011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'BGE'.lower():
		# BGE	rs1	rs2	imm
		rs1 = Bits(int=int(instr[1][1:]), length=5).bin
		rs2 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm[0] + imm[2:8] + rs2 + rs1 + "101" + imm[8:12] + imm[1] + "1100011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'BLTU'.lower():
		# BLTU	rs1	rs2	imm
		rs1 = Bits(int=int(instr[1][1:]), length=5).bin
		rs2 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm[0] + imm[2:8] + rs2 + rs1 + "110" + imm[8:12] + imm[1] + "1100011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'BGEU'.lower():
		# BGEU	rs1	rs2	imm
		rs1 = Bits(int=int(instr[1][1:]), length=5).bin
		rs2 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm[0] + imm[2:8] + rs2 + rs1 + "111" + imm[8:12] + imm[1] + "1100011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'LB'.lower():
		# LB	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "000" + rd + "0000011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'LBU'.lower():
		# LBU	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "100" + rd + "0000011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'LH'.lower():
		# LH	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "001" + rd + "0000011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'LHU'.lower():
		# LHU	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "101" + rd + "0000011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'LW'.lower():
		# LW	rd	rs1	imm	//load (rs1+imm) to rd
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "010" + rd + "0000011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SB'.lower():
		# SB	rs1	rs2	imm	//store rs2 to to (rs1+imm)
		rs1 = Bits(int=int(instr[1][1:]), length=5).bin
		rs2 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm[0:7] + rs2 + rs1 + "000" + imm[7:12] + "0100011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SH'.lower():
		# SH	rs1	rs2	imm
		rs1 = Bits(int=int(instr[1][1:]), length=5).bin
		rs2 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm[0:7] + rs2 + rs1 + "001" + imm[7:12] + "0100011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SW'.lower():
		# SW	rs1	rs2	imm
		rs1 = Bits(int=int(instr[1][1:]), length=5).bin
		rs2 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm[0:7] + rs2 + rs1 + "010" + imm[7:12] + "0100011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'ADDI'.lower():
		# ADDI	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "000" + rd + "0010011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SLTI'.lower():
		# SLTI	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "010" + rd + "0010011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SLTIU'.lower():
		# SLTIU	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "011" + rd + "0010011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'XORI'.lower():
		# XORI	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "100" + rd + "0010011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'ORI'.lower():
		# ORI	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "110" + rd + "0010011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'ANDI'.lower():
		# ANDI	rd	rs1	imm
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=12).bin
		mach_code = imm + rs1 + "111" + rd + "0010011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SLLI'.lower():
		# SLLI	rd	rs1	imm	//imm=shamt
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=5).bin
		mach_code = "0000000" + imm + rs1 + "001" + rd + "0010011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SRLI'.lower():
		# SRLI	rd	rs1	imm	//imm=shamt
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=5).bin
		mach_code = "0000000" + imm + rs1 + "101" + rd + "0010011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SRAI'.lower():
		# SRAI  rd	rs1	imm	//imm=shamt
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		imm = Bits(int=int(instr[3]), length=5).bin
		mach_code = "0100000" + imm + rs1 + "101" + rd + "0010011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'ADD'.lower():
		# ADD	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0000000" + rs2 + rs1 + "000" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SUB'.lower():
		# SUB	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0100000" + rs2 + rs1 + "000" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SLL'.lower():
		# SLL	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0000000" + rs2 + rs1 + "001" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SLT'.lower():
		# SLT	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0000000" + rs2 + rs1 + "010" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SLTU'.lower():
		# SLTU	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0000000" + rs2 + rs1 + "011" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'XOR'.lower():
		# XOR	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0000000" + rs2 + rs1 + "100" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SRL'.lower():
		# SRL	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0000000" + rs2 + rs1 + "101" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'SRA'.lower():
		# SRA	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0100000" + rs2 + rs1 + "101" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'OR'.lower():
		# OR	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0000000" + rs2 + rs1 + "110" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	elif instr[0].lower() == 'AND'.lower():
		# AND	rd	rs1	rs2
		rd = Bits(int=int(instr[1][1:]), length=5).bin
		rs1 = Bits(int=int(instr[2][1:]), length=5).bin
		rs2 = Bits(int=int(instr[3][1:]), length=5).bin
		mach_code = "0000000" + rs2 + rs1 + "111" + rd + "0110011" + '\t' + "//address" + str(addr) + ":\t" + line
		outfile.write(mach_code + '\n')

	else:
		print("Error!! Unknown instruction:  ", line)
		break
        addr = addr + 4;

outfile.write(terminator.bin + '\t' + "//address" + str(addr) + ":\t" + "Invalid instruction - indicates stop\n")
outfile.close()
infile.close()
