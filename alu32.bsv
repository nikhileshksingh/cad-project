`include "global_parameters"
package alu32;

//import packages
import global_definations :: *;

//export variables

//interface declarations

//constant declarations

//function declarations
(*noinline*)
function AluOut perform_aluop(AluOp aluop);
        AluOut result;
        DataInt sign_op1, sign_op2;
        DataUInt usign_op1, usign_op2;
        Bool is_op_pc = False;

        result.error = False;
        result.branchcond = False;

        //Prepare operands
        if (aluop.operand1 matches tagged Indata .op1) begin
            sign_op1 = unpack(op1);
            usign_op1 = unpack(op1);
        end else if (aluop.operand1 matches tagged Pc .op1) begin
            sign_op1 = unpack(op1);
            usign_op1 = unpack(op1);
            is_op_pc = True;
        end else begin
            result.error = True;
            sign_op1 = 0;   //dummy
            usign_op1 = 0;  //dummy
        end

        if (aluop.operand2 matches tagged Indata .op2) begin
                sign_op2 = unpack(op2);
            if (!is_op_pc) begin
                usign_op2 = unpack(op2);
            end else begin
                //We need to add or subtract immediate to/from pc
                //if sign2 = -10; usign2 = 10; then do aluout = aluop1 - usign2
                if (1'b1 == msb(op2))
                    usign_op2 = unpack(pack(-sign_op2));
                else
                    usign_op2 = unpack(op2);
            end
        end else begin
            result.error = True;
            sign_op2 = 0;   //dummy
            usign_op2 = 0;  //dummy
        end

        case (aluop.operation)
            Add:
            begin
                if (!is_op_pc)
                    result.aluout = pack(sign_op1 + sign_op2);   //Ignore overflow/underflows
                else begin
                    //We are performing operations on PC. So aluop1 should be unsigned while aluop2 is signed
                    if (1'b1 == msb(sign_op2))
                        result.aluout = pack(usign_op1 - usign_op2);
                    else
                        result.aluout = pack(usign_op1 + usign_op2);
                end
            end

            Sub:
            begin
                result.aluout = pack(sign_op1 - sign_op2);   //Ignore overflows/underflows
            end

            Eq:
            begin
                result.branchcond = (usign_op1 == usign_op2);    //Since equality comparision is independent of sign
                result.aluout = zeroExtend(pack(result.branchcond));
            end

            NEq:
            begin
                result.branchcond = (usign_op1 != usign_op2);    //Since inequality comparision is independent of sign
                result.aluout = zeroExtend(pack(result.branchcond));
            end

            Lt:
            begin
                result.branchcond = (sign_op1 < sign_op2);
                result.aluout = zeroExtend(pack(result.branchcond));
            end

            LtU:
            begin
                result.branchcond = (usign_op1 < usign_op2);
                result.aluout = zeroExtend(pack(result.branchcond));
            end

            Ge:
            begin
                result.branchcond = (sign_op1 >= sign_op2);
                result.aluout = zeroExtend(pack(result.branchcond));
            end

            GeU:
            begin
                result.branchcond = (usign_op1 >= usign_op2);
                result.aluout = zeroExtend(pack(result.branchcond));
            end

            Or:
            begin
                result.aluout = pack(usign_op1) | pack(usign_op2);
            end

            And:
            begin
                result.aluout = pack(usign_op1) & pack(usign_op2);
            end

            Xor:
            begin
                result.aluout = pack(usign_op1) ^ pack(usign_op2);
            end

            LLS:
            begin
                Data numbits = fromInteger(valueof(`RegIndexSz));
                Data shamt = pack(usign_op2)[numbits-1:0];
                result.aluout = pack(usign_op1 << shamt);
            end

            LRS:
            begin
                Data numbits = fromInteger(valueof(`RegIndexSz));
                Data shamt = pack(usign_op2)[numbits-1:0];
                result.aluout = pack(usign_op1 >> shamt);
            end

            ARS:
            begin
                Data numbits = fromInteger(valueof(`RegIndexSz));
                Data shamt = pack(usign_op2)[numbits-1:0];
                result.aluout = pack(sign_op1 >> shamt);
            end

            Mul:
            begin
                result.aluout = 0; //pack(sign_op1*sign_op2);
                result.error = True; //Mul is unsupported as of now
            end

            Div:
            begin
                result.aluout = 0; //pack(sign_op1/sign_op2);
                result.error = True; //Div is unsupported as of now
            end

            default:
            begin
                result.aluout = 0; //Invalid
                result.error = True;
            end
        endcase

        return result;
endfunction

//module declarations

endpackage: alu32
