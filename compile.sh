#!/bin/bash
# Compile the processor code:
#bsc -keep-fires -aggressive-conditions -no-warn-action-shadowing -show-range-conflict -sim -g mkProcessor32 processor32.bsv

# Compilation of testbench for simulation:
bsc -show-schedule -check-assert -keep-fires -aggressive-conditions -no-warn-action-shadowing -show-range-conflict -sim global_definations.bsv
bsc -show-schedule -check-assert -keep-fires -aggressive-conditions -no-warn-action-shadowing -show-range-conflict -sim decode32.bsv
bsc -show-schedule -check-assert -keep-fires -aggressive-conditions -no-warn-action-shadowing -show-range-conflict -sim alu32.bsv
bsc -show-schedule -check-assert -keep-fires -aggressive-conditions -no-warn-action-shadowing -show-range-conflict -sim -g mkProcessor32 processor32.bsv
bsc -show-schedule -check-assert -keep-fires -aggressive-conditions -no-warn-action-shadowing -show-range-conflict -sim -g mkTestbench testbench.bsv
bsc +RTS -k20M -RTS -check-assert -sim -e mkTestbench -o testbench mkTestbench.ba mkProcessor32.ba

# Delete unnecessary simulation files
rm -rf *.ba
rm -rf *.bo
rm -rf *.cxx
rm -rf *.h
rm -rf *.o
rm -rf *.sched
