##default: realclean  compile  link
##
##BSCFLAGS = -keep-fires -aggressive-conditions -no-warn-action-shadowing -show-range-conflict
##
##BSV_PRG_NAME=testbench
##BSV_TOP_MODULE=mkTestbench
##BSV_PROC_MODULE=mkProcessor32
##BSV_PROC_FILE_NAME=processor.bsv
##BSV_SRC_DIR = ~/workspace/cad-project
##BSV_BUILD_DIR = $(BSV_SRC_DIR)/build_bsim
##
##.PHONY: compile
##compile:
##	@echo Compiling...
##	@mkdir -p $(BSV_BUILD_DIR)
##	bsc -u -sim -simdir $(BSV_BUILD_DIR) -bdir $(BSV_BUILD_DIR) -info-dir $(BSV_BUILD_DIR) $(BSCFLAGS) \
##		-g $(BSV_PROC_MODULE) $(BSV_SRC_DIR)/$(BSV_PROC_FILE_NAME)
##	bsc -u -sim -simdir $(BSV_BUILD_DIR) -bdir $(BSV_BUILD_DIR) -info-dir $(BSV_BUILD_DIR) $(BSCFLAGS) \
##		-g $(BSV_TOP_MODULE) $(BSV_SRC_DIR)/$(BSV_PRG_NAME).bsv
##	@echo Compilation finished
##
##.PHONY: link
##link:
##	@echo Linking...
##	bsc -e $(BSV_TOP_MODULE) -sim -o ./out -simdir $(BSV_BUILD_DIR) $(BSV_BUILD_DIR)/$(BSV_TOP_MODULE).ba $(BSV_BUILD_DIR)/$(BSV_PROC_MODULE).ba 
##	@echo Linking finished
##
##.PHONY: simulate
##simulate:
##	@echo Simulation...
##	./out 
##	@echo Simulation finished
##
### ----------------------------------------------------------------
##
##.PHONY: clean
##clean:
##	rm -f  *~  BSV_SRC_DIR/*~  $(BSV_BUILD_DIR)/*
##
##.PHONY: realclean
##realclean:
##	rm -f  *~  BSV_SRC_DIR/*~  $(BSV_BUILD_DIR)/*
##	rm -f  out  out.so
##	rm -rf $(BSV_BUILD_DIR)
