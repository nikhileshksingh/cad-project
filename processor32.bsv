`include "global_parameters"
package processor32;

//import packages
import Assert :: *;
import Vector :: *;
import RegFile :: *;
import GetPut :: *;
import DReg :: *;
import global_definations :: *;
import decode32 :: *;
import alu32 :: *;
import ConfigReg :: * ;

//export variables

//interface declaration

//constants declaration

//function declaration

//module declaration
(*synthesize*)
module mkProcessor32 #(Address start_pc) (ProcessorIFC)
                     provisos(Bits#(Instruction, x),
                              Bits#(CInstruction, y),
                              Mul#(y,2,x));

    //Constant declerations

    //Module instantiations
    //Define generic items
    Reg#(Address) pc <- mkReg(0); //start_pc - can't use this since dynamic parameter can't be used for instantiation
    Reg#(Address) prev_pc <- mkReg(0); //pc of instruction in decode phase
    //RegFile#(RegIndex, Data) regfile <- mkRegFileWCF(0, fromInteger(valueof(`NumRegs)-1)); //Make reads and writes conflict free
    Vector#(`NumRegs, Reg#(Data)) regfile_v <- replicateM(mkConfigRegU);
    Reg#(Bool) started <- mkReg(False);
    Reg#(Bool) fetch_started <- mkReg(False);
    Reg#(Bool) decode_started <- mkReg(False);
    Reg#(Bool) alu_started <- mkReg(False);
    Reg#(Bool) mem_started <- mkReg(False);
    Reg#(Bool) pc_incr_started <- mkReg(False);
    Wire#(Bool) invalid_instr <- mkDWire(False); //Indicates if instruction decoded in current cycle is valid or not
    Wire#(Bool) alu_error <- mkDWire(False); //Indicates any errors from alu
    Wire#(Bool) imem_error <- mkDWire(False);//Indicates any errors while fetching instructions
    Wire#(Bool) dmem_error <- mkDWire(False);//Indicates any errors while loading from/writing to data memory
    Wire#(Bool) is_c_instr <- mkDWire(False); //Indicates that current instruction decoded in a compressed instruction
    Reg#(Bool) is_c_instr_reg <- mkDReg(False);
    Reg#(Bool) instr_alligned_with_pc <- mkReg(False);  //True if decodes are not aligned with the current pc boundary
    Reg#(Bool) instr_alligned_with_pc_wr <- mkDWire(False);
    Reg#(Maybe#(Address)) branch_address_reg <- mkDReg(tagged Invalid);  //used for flushing instructions after branch
    Wire#(Maybe#(Address)) branch_address <- mkDWire(tagged Invalid);    //used to change pc once a branch is detected
    Wire#(Bool) data_hazard <- mkDWire(False);
    Wire#(Bool) data_hazard_prev <- mkDWire(False); //Same as data_hazard_reg - added to deal with scheduling conflicts
    Reg#(Bool) data_hazard_reg <- mkDReg(False);
    Reg#(RegIndex) alu_regidx <- mkDReg (0);  //For data forwarding purposes - indicates rd of instruction in alu phase - can't use wires - sched conflict
    Reg#(RegIndex) mem_regidx <- mkDReg(0);   //For data forwarding purposes - indicates rd of instruction in mem phase - if rd is 0, ignore since reg[0] = 0 always
    Reg#(RegIndex) regwb_regidx <- mkDReg(0); //For data forwarding purposes - indicates rd of instruction in wb phase

    //Define items specific to this implementation
    Wire#(Instruction) cur_instruction <- mkDWire(0);   //from fetch
    Reg#(Instruction) prev_instruction <- mkReg(0);    //Instruction fetched in previous cycle
    Reg#(Instruction) prev_prev_instruction <- mkReg(0);    //The instruction that should be in alu phase - used for dealing with hazards when compressed isa is enabled
    Reg#(Maybe#(DecodedInstruction)) decoded_instruction <- mkReg(tagged Invalid);    //from decode
    Reg#(Maybe#(AluToMemInstruction)) alutomem_instruction <- mkReg(tagged Invalid);  //from alu
    Reg#(Maybe#(MemToWbInstruction)) memtowb_instruction <- mkReg(tagged Invalid);    //from mem
    
    Wire#(Proc2DMemReq) memdatareq <- mkWire();
    Wire#(Data) memdataresp <- mkDWire(0);

    //Function declarations

    //Rules
    rule start (!started);
        //Wait for one cycle to initialize pc
        pc <= start_pc;
        //regfile.upd(0, 0);
        regfile_v[0] <= 0;

        started <= True;
`ifdef Simulation
        $display("Loading start_pc: %d", start_pc);
`endif
    endrule: start

    //fetch is taken care of by proc2IMemIFC.request.get and proc2IMemIFC.response.put interfaces.
    rule fetch_start (started && !fetch_started);
        //Wait for one additional cycle for fetching the instruction
        fetch_started <= True;
`ifdef Simulation
        $display("Starting fetch");
`endif
    endrule: fetch_start

    rule decode (fetch_started);
        //Check if we are after branch - need to flush next instruction
        Bool stall = False;
        Bool cur_inst_is_c = False;

        if (isValid(branch_address_reg)) begin
            //Next instruction after branch should be flushed since decode is immediately after fetch
            decoded_instruction <= tagged Invalid;
            instr_alligned_with_pc <= False;
`ifdef Simulation
            $display("Decode: Had a branch stall - discarding the instruction");
`endif
        end else begin
            //Note: We treat any invalid instruction as NOP and we don't update regfile, memory and pc <= pc + 4;
            DecodeFnOut decodefnout;
            Instruction instruction;// = data_hazard_reg ? prev_instruction : cur_instruction;
            Address instr_pc;// = is_c_instr_reg ? prev_pc + 2 : prev_pc;
            //Possible scenarios: - pc is always on 32 bit boundary unless some jump happens to a 16 it boundary
            //Currently instruction is on a 32 bit boundary and next instruction is compressed - use prev_pc and cur_instruction[15:0]
            //Current instruction is on a 32 bit boundary and next instruction is not compressed - use prev_pc and cur_instruction
            //Current instruction is on a 16 bit boundary and next instruction is compressed - use prev_pc + 2 and prev_instruction[31:16]
            //Current instruction is on a 16 bit boundary and next instruction is not compressed - use prev_pc + 2 and cur_instruction[15:0]prev_instruction[31:16]
            data_hazard_prev <= data_hazard_reg;
            instr_alligned_with_pc_wr <= instr_alligned_with_pc;
            if (data_hazard_reg) begin
                //We had a data hazard. We need to use previous instruction
                if(instr_alligned_with_pc) begin
                    instruction = unpack({prev_instruction[15:0], prev_prev_instruction[31:16]});
                    instr_pc = prev_pc - 2;
                end else begin
                    //Not on 16 bit boundary. Normal run
                    instruction = prev_instruction;
                    instr_pc = prev_pc;
                end
            end else begin
               if (instr_alligned_with_pc) begin
                   //On 16 bit boundary
                   instruction = unpack({cur_instruction[15:0], prev_instruction[31:16]});
                   instr_pc = prev_pc - 2;
               end else begin
                   //Not on 16 bit boundary. Normal run
                   instruction = cur_instruction;
                   instr_pc = prev_pc;
               end
            end

            decodefnout <- decode_cinstruction(instruction[`CInstrSz-1:0], regfile_v, instr_pc, alu_regidx, mem_regidx, regwb_regidx);
            if (decodefnout.decoded_instr matches tagged Invalid)  //Instruction is not compressed
                decodefnout <- decode_instruction(instruction, regfile_v, instr_pc, alu_regidx, mem_regidx, regwb_regidx);
            else begin
                is_c_instr <= True;
                is_c_instr_reg <= True;
                instr_alligned_with_pc <= instr_alligned_with_pc ? False : True; //If we are on 16 bit boundary and encounter another c-instr, make it false and vice-versa
                cur_inst_is_c = True;
            end
            Maybe#(DecodedInstruction) dec_instr = decodefnout.decoded_instr;

            if (dec_instr matches tagged Valid .instr) begin
                if (decodefnout.data_hazard) begin
                    //Since the code is executed in-order, only RAW hazards can occur
                    decoded_instruction <= tagged Invalid;
                    data_hazard <= True;
                    data_hazard_reg <= True;
                    stall = True;
                end else begin
                    decoded_instruction <= dec_instr;
                    if (instr.regwrop matches tagged Valid .regwrop) begin
                        alu_regidx <= regwrop.writereg;
                    end else begin
                        alu_regidx <= 0;
                    end
                end
            end else begin
                invalid_instr <= True;
                decoded_instruction <= tagged Invalid;
            end
        end

        //1. If both data_hazard_reg and stall are true, the data hazard is not yet cleared. So wait.
        //2. If the data hazard is just cleared but we only processed 1st half of the previous instruction, wait
        //3. If we processed only 2nd half of last instruction, wait so that we won't always keep processing previous instruction
        if (!((data_hazard_reg && stall) || (data_hazard && !stall && !instr_alligned_with_pc && cur_inst_is_c) || (cur_inst_is_c && instr_alligned_with_pc)))
            prev_instruction <= cur_instruction;
        if (stall && instr_alligned_with_pc)
            prev_prev_instruction <= prev_instruction;
        decode_started <= True;
    endrule: decode

    rule do_alu (decode_started);
        if (isValid(branch_address_reg)) begin
            //The two instructions following branch should be flushed w.r.t.
            //alu stage since it is the 2nd stage after fetch. However, since
            //the 2nd instruction after branch is made invalid by decode stage,
            //we just need to flush the 1st instruction after branch.
            alutomem_instruction <= tagged Invalid;
`ifdef Simulation
            $display("Alu: Had a branch stall - discarding the instruction");
`endif
        end else if (decoded_instruction matches tagged Valid .instruction) begin
            AluToMemInstruction instr;
            instr.memop = instruction.memop;
            instr.regwrop = instruction.regwrop;
            instr.wrdata_or_memaddr = instruction.wrdata_or_memaddr;

            if (instruction.aluop matches tagged Valid .aluop) begin
                AluOut out = perform_aluop(aluop);
                alu_error <= out.error;

                if (instruction.pcchangeop matches tagged Valid .pcchangeop) begin
                    case (pcchangeop.operation)
                        Jal:
                        begin
                            branch_address <= tagged Valid ((pcchangeop.baseaddress + unpack(pack(pcchangeop.offset))) & {28'hfffffff,4'b1110}); //Need to set last bit to 0
                            branch_address_reg <= tagged Valid ((pcchangeop.baseaddress + unpack(pack(pcchangeop.offset))) & {28'hfffffff,4'b1110}); //Need to set last bit to 0
                        end

                        Branch:
                        begin
                            if(out.branchcond)
                                branch_address <= tagged Valid (pcchangeop.baseaddress + unpack(pack(pcchangeop.offset)));
                                branch_address_reg <= tagged Valid (pcchangeop.baseaddress + unpack(pack(pcchangeop.offset)));
                        end

                        default: dynamicAssert(False, "Unknown Jump/Branch Instruction");
                    endcase
                end

                //Put alu out into wrdata_or_memaddr
                if (isValid(instr.memop)) begin
                    instr.wrdata_or_memaddr = tagged MemAddress unpack(pack(out.aluout));
                end else if (isValid(instr.regwrop)) begin
                    if (instr.wrdata_or_memaddr matches tagged Invalid)
                        instr.wrdata_or_memaddr = tagged WrData out.aluout;
                end
`ifdef Simulation
                $display("Alu: out = %d branch = %d", out.aluout, out.branchcond);
`endif
            end else begin
                if (isValid(instruction.pcchangeop)) dynamicAssert(False, "Unknown operation: PC change invovled without Alu operation");
`ifdef Simulation
                $display("Alu: No Alu Op");
`endif
            end

            alutomem_instruction <= tagged Valid instr;
        end else begin
            alutomem_instruction <= tagged Invalid;
`ifdef Simulation
            $display("Alu: NoOp");
`endif
        end
        alu_started <= True;
    endrule: do_alu

    rule increment_pc(started);
        if (branch_address matches tagged Valid .address) begin
            prev_pc <= pc; //Just for completeness sake. This is not needed as next few instructions will be discarded.
            pc <= address;
`ifdef Simulation
            $display("Fetch: Detected a branch. Moving to pc: %d", address);
`endif
        end else if (data_hazard) begin
            prev_pc <= prev_pc;
            pc <= pc;
`ifdef Simulation
            $display("Fetch: Stalling pc increment");
`endif
        end else if ((!data_hazard && data_hazard_prev && !instr_alligned_with_pc_wr && is_c_instr) || (is_c_instr && instr_alligned_with_pc_wr)) begin
            prev_pc <= prev_pc;
            pc <= pc;
        end else begin
            prev_pc <= pc;
            pc <= pc + 4;
        end
        pc_incr_started <= True;

        //shift any pipeline registers
        mem_regidx <= alu_regidx;
        regwb_regidx <= mem_regidx;
    endrule: increment_pc

    rule do_mem (alu_started);
        if (alutomem_instruction matches tagged Valid .instruction)
            if (instruction.memop matches tagged Valid .memop) begin
                Proc2DMemReq request;
                //Send a memory request;
                if (instruction.wrdata_or_memaddr matches tagged MemAddress .addr) begin
                    request.addr = addr;
                end else begin
                    dynamicAssert(False, "Unknown type wrdata_or_memaddr in memory phase");
                    request.addr = 0;
                end

                request.data = memop.storedata;
                request.width = memop.datawidth;
                case (memop.operation)
                    Load:  request.rw = Read;
                    Store: request.rw = Write;
                    default: request.rw = Read; //Dummy
                endcase
                memdatareq <= request;

                //Prepare data to be sent to write back phase
                memtowb_instruction <= tagged Valid MemToWbInstruction {dw:memop.datawidth, ds:memop.datasign, regwrop:instruction.regwrop, wrdata_or_memaddr:instruction.wrdata_or_memaddr};
`ifdef Simulation
                $display("Mem: mem request: R/W: %s, addr = %d, data = %d, width = %d", request.rw == Write ? "Write" : "Read", request.addr, request.data, request.width);
`endif
            end else begin
                memtowb_instruction <= tagged Valid MemToWbInstruction {dw:W, ds:S, regwrop:instruction.regwrop, wrdata_or_memaddr:instruction.wrdata_or_memaddr};
`ifdef Simulation
                $display("Mem: No Mem Op");
`endif
            end
        else begin
            memtowb_instruction <= tagged Invalid;
`ifdef Simulation
            $display("Mem: NoOp");
`endif
        end
        mem_started <= True;
    endrule: do_mem

    rule write_back (mem_started && started);
        if (memtowb_instruction matches tagged Valid .instruction) begin
            if(instruction.regwrop matches tagged Valid .regwrop) begin
                Data writedata;
                if (regwrop.operand == Mem) begin
                    if (instruction.dw == W) writedata = memdataresp;
                    else if ((instruction.dw == B) && (instruction.ds == S)) writedata = signExtend(memdataresp[7:0]);
                    else if ((instruction.dw == H) && (instruction.ds == S)) writedata = signExtend(memdataresp[15:0]);
                    else if ((instruction.dw == B) && (instruction.ds == U)) writedata = zeroExtend(memdataresp[7:0]);
                    else if ((instruction.dw == H) && (instruction.ds == U)) writedata = zeroExtend(memdataresp[15:0]);
                    else begin writedata = 0; dynamicAssert(False, "Invalid datawidth/datasign during write back from memory"); end
                end else begin
                    if (instruction.wrdata_or_memaddr matches tagged WrData .wrdata)
                        writedata = wrdata;
                    else begin writedata = 0; dynamicAssert(False, "Invalid wrdata_or_memaddr in write back phase"); end
                end
                if (0 != regwrop.writereg) regfile_v[regwrop.writereg] <= writedata;//regfile.upd(regwrop.writereg, writedata);
`ifdef Simulation
                if (0 != regwrop.writereg) $display("WB: writedata = %d writereg = %d", writedata, regwrop.writereg);
                else $display("rd = 0. Not writing");
`endif
            end else begin
`ifdef Simulation
                $display("WB: No write back Op");
`endif
            end
        end else begin
`ifdef Simulation
            $display("WB: NoOp");
`endif
        end
    endrule: write_back

    //Methods
    method Bool isInvalidInsr() if(fetch_started);
        return invalid_instr;
    endmethod

    method Bool isAluError if(decode_started);
        return alu_error;
    endmethod

    method Bool isIMemError if(fetch_started);
        return imem_error;
    endmethod

    method Bool isDMemError if(mem_started);
        return dmem_error;
    endmethod

    method Bool isProcStalled() if (started);
        return isValid(branch_address_reg) || data_hazard_reg;
    endmethod: isProcStalled

    //Subinterfaces
    //    interface Client#(Address, Instruction) proc2IMemIFC;
    //    interface Client#(Proc2DMemReq, Data) proc2DMemIFC;
    interface ClientValueGet proc2IMemIFC;
        interface ValueGet request;
            method Address get() if(started);
                return pc;
            endmethod: get
        endinterface: request

        interface Put response;
            method Action put(IMem2ProcResp resp) if(started);
                cur_instruction <= resp.instr;
                imem_error <= resp.error;
            endmethod: put
        endinterface: response
    endinterface: proc2IMemIFC

    interface ClientValueGet proc2DMemIFC;
        interface ValueGet request;
            method Proc2DMemReq get() if(started);
                let req = memdatareq;
                return req;
            endmethod: get
        endinterface: request

        interface Put response;
            method Action put(DMem2ProcResp resp) if(started);
                memdataresp <= resp.data;
                dmem_error <= resp.error;
            endmethod: put
        endinterface: response
    endinterface: proc2DMemIFC
endmodule:mkProcessor32
endpackage: processor32
