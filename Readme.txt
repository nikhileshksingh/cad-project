For compilation:
./compile.sh

For execution:
./testbench
(takes input from ./test/machine_code)

Sample Input:
./test/sample_input

Expected output:
Stored in file "./test/sample_input_expected_output"


Convert from assembly to machine code:
./test.convert.py -infile <> -outfile <>
(Default infile is "./test/assembly_code" and outfile is "./test/machine_code")

For cleaning:
rm -rf testbench testbench.so
